<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar() {
        return view ('page.register');
    }

    public function datang(Request $request) {
       
      
         $nama_depan = $request['first_name'];
         $nama_belakang = $request['last_name'];

         return view('page.welcome', compact("nama_depan", "nama_belakang"));
    }
}
