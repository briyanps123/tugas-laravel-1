@extends('layout.master')
@section('title')
    Halaman Form
@endsection
@section('content')
<h4>Buat Account Baru <br></h4>
<h5>Sign Up Form <br></h5>
<form action="/welcome" method="POST">
    @csrf
    <label>First name :</label><br><br>
        <input type="text" name="first_name"><br><br>
    <label>Last Name :</label><br><br>
        <input type="text" name="last_name"><br><br>
    <label>Gender</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio"name="gender">Female<br><br>
    <label>Nationality</label><br><br>
    <select name="nationality" id="">
        <option value = "1">Indonesia</option>
        <option value = "2">Amerika</option>
        <option value = "3">Inggris</option>
    </select><br><br>
    <label>Language Spoken</label><br><br>
        <input type = "checkbox" name = "bahasa">Bahasa Indonesia<br>
        <input type = "checkbox" name = "bahasa">English<br>
        <input type = "checkbox" name = "bahasa">Other<br><br>
    <label>Bio</label><br><br>
        <textarea name="bio" cols = "30" rows ="10"></textarea><br>
    <input type = "submit" value = "Sign Up" >

</form>
@endsection
